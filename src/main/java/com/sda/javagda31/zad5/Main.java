package com.sda.javagda31.zad5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.addAll(Arrays.asList(
                new Student("123", "Marian", "Kowalski", Plec.MEZCZYZNA),
                new Student("342", "Katarzyna", "Kowalska", Plec.KOBIETA),
                new Student("568", "Włodek", "Smith", Plec.MEZCZYZNA),
                new Student("367", "Marcin", "Nowak", Plec.MEZCZYZNA),
                new Student("273", "Olek", "Dębski", Plec.MEZCZYZNA),
                new Student("299", "Juliusz", "Brzęczyszczykiewicz", Plec.MEZCZYZNA),
                new Student("929", "Maryla", "Dębska", Plec.KOBIETA)
        ));

        System.out.println();
        for (Student student : students) {
            System.out.println(student);
        }

        System.out.println("Tylko kobiety: ");
        for (Student student : students) {
            if (student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

        System.out.println("Tylko mezczyzni: ");
        for (Student student : students) {
            if (student.getPlec() == Plec.MEZCZYZNA) {
                System.out.println(student);
            }
        }

        System.out.println("Tylko indeksy: ");
        for (Student student : students) {
            System.out.println(student.getIndeks());
        }
        // "abecadło"
        // " nic"

        // 1. przydzielenie pamięci
        // 2. przepisanie 1 ciągu znaków
        // 3. przepisanie 2 ciągu znaków
        //
        // Rozwiązanie:
        // StringBuilder - rezerwuje pamięć i zakłada że będziemy konkatenować wiele stringów
        // .append
        //


    }
}
