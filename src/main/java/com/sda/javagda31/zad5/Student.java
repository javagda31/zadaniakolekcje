package com.sda.javagda31.zad5;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Student {
    private String indeks;
    private String imie;
    private String nazwisko;
    private Plec plec;
//    private enum plec; < -- BŁĄD - bebe, nie wolno!

    public Student(String indeks, String imie, String nazwisko, Plec plec) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }
}
