package com.sda.javagda31.zad6;

import com.sda.javagda31.zad5.Plec;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

@Getter
@Setter
@ToString
public class Student {
    private String indeks;
    private String imie;
    private String nazwisko;
    private List<Double> oceny;

    public Student(String indeks, String imie, String nazwisko) {
        this.indeks = indeks;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.oceny = new ArrayList<>();
    }

    public void dodajOcene(Double ocena) {
        this.oceny.add(ocena);
    }

    public Optional<Double> obliczSrednia() {
        double sum = 0.0;
        for (Double ocena : oceny) {
            sum += ocena;
        }

        if (oceny.size() > 0) {
            return Optional.of(sum / oceny.size());
        }
        return Optional.empty();
    }

    public OptionalDouble obliczSrednia2() {
        return oceny.stream()
                .mapToDouble(aDouble -> aDouble)
                .average(); // OptionalDouble
    }
}
