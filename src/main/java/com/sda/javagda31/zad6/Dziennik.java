package com.sda.javagda31.zad6;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Dziennik {
    private List<Student> studentList = new ArrayList<>();

    //    todo: dalej piszemy metody
    public void dodajStudenta(Student st) {
        studentList.add(st);
    }

    public void usunStudenta(Student st) {
        studentList.remove(st);
    }

    public void usunStudenta(String index) {
        for (Student student : studentList) {           // iteracja po studentach
            if (student.getIndeks().equals(index)) {      // jeśli znalazłem studenta z indeksem takim jak w parametrze
                studentList.remove(student);            // usuwam studenta z listy

                break; // bez break pętla wyrzuci "Concurrent Modification Exception"
                // dodanie break powoduje "wyskoczenie" z pętli tuż po
                // znalezieniu i usunięciu rekordu (studenta).
                // Dzięki temu iteracja się przerywa i nie dochodzi do błędu iteratora.
            }
        }
    }

//    public Student zwrocStudenta(String index) {
//        for (Student student : studentList) {
//            if (student.getIndeks().equals(index)) {
//                return student;
//            }
//        }
//        return null;
//    }

    public Optional<Student> zwrocStudenta(String index) {
        for (Student student : studentList) {
            if (student.getIndeks().equals(index)) {
                return Optional.of(student); // pełne pudełko - w pudełku jest Student
            }
        }
        return Optional.empty(); // puste pudełko
    }

    public Optional<Double> podajSredniaStudenta(String index) {
        Optional<Student> studentOptional = zwrocStudenta(index); // szukam studenta
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get(); // jeśli udało się znaleźć studenta

            return student.obliczSrednia(); // wykonujemy metodę która zwraca średnią
        }
        return Optional.empty(); // jeśli nie udało się odnaleźć studenta, zwracamy nic.
    }

    public List<Student> podajStudentówZagrozonych() {
        List<Student> zagrozeni = new ArrayList<>();
        for (Student student : studentList) {
            Optional<Double> srednia = student.obliczSrednia();
            if (srednia.isPresent() && srednia.get() <= 2.5) {
                zagrozeni.add(student);
            }
        }
        return zagrozeni;
    }

    public List<Student> podajStudentówZagrozonych2() {
        return studentList.stream()
                .filter(student -> {
                    Optional<Double> srednia = student.obliczSrednia();
                    // oblicz średnią i sprawdź czy student ma średnią, jeśli ma to
                    // sprawdź czy jest niższa od 2.5
                    return srednia.isPresent() && srednia.get() <= 2.5;
                }).collect(Collectors.toList());
    }


}
