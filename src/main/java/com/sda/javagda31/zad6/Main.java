package com.sda.javagda31.zad6;


import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.addAll(Arrays.asList(
                new Student("123", "Marian", "Kowalski"),
                new Student("342", "Katarzyna", "Kowalska"),
                new Student("568", "Włodek", "Smith"),
                new Student("367", "Marcin", "Nowak"),
                new Student("273", "Olek", "Dębski"),
                new Student("299", "Juliusz", "Brzęczyszczykiewicz"),
                new Student("929", "Maryla", "Dębska")
        ));

        Random generator = new Random();

        Dziennik dziennik = new Dziennik();
        for (Student student : students) {
            dziennik.dodajStudenta(student);

            // ile ocen chcemy wygenerować temu studentowi
            int ileOcenWygenerować = generator.nextInt(5);

            for (int i = 0; i < ileOcenWygenerować; i++) {
//                double wygenerowane = (generator.nextDouble()*4) + 2; // 0-1
//                student.dodajOcene(wygenerowane);

                double wygenerowane = generator.nextInt(4) + 2;// 0, 1, 2, 3
                student.dodajOcene(wygenerowane);
            }
        }
        Optional<Double> ocenaOptional = dziennik.podajSredniaStudenta("123");
        if (ocenaOptional.isPresent()) {
            System.out.println("Średnia: " + ocenaOptional.get());
        }else{
            System.err.println("Błąd, nie znaleziono ocen.");
        }
    }
}
