package com.sda.javagda31.designpatterns.factory;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    private String name;
    private String model;
    private double power;
}
