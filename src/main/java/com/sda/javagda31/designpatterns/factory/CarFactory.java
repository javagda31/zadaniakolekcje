package com.sda.javagda31.designpatterns.factory;

// abstract factory
public abstract class CarFactory {

    // factory method
    public static Car createBMW16(){
        return new Car("BMW", "e36", 100);
    }

    public static Car createMaluch(){
        return new Car("Fiat", "126p", 110);
    }
}
