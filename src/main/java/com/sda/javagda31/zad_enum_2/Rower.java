package com.sda.javagda31.zad_enum_2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Rower {
    private int iloscPrzerzutek;
    private TypRoweru typRoweru;
    private String nazwaRoweru;

    @Override
    public String toString() {
        return "Rower{" +
                "iloscPrzerzutek=" + iloscPrzerzutek +
                ", ilosc miejsc=" + typRoweru.getIloscMiejsc()+
                ", typRoweru=" + typRoweru  +
                ", nazwaRoweru='" + nazwaRoweru + '\'' +
                '}';
    }

    public void wypiszInformacjeORowerze(){
        System.out.println(this);
    }
}
