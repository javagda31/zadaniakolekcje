package com.sda.javagda31.zad_enum_2;

public class Main {
    public static void main(String[] args) {
        Rower rower1 = new Rower(5, TypRoweru.TANDEM, "za szybki");
        Rower rower2 = new Rower(5, TypRoweru.ROWER, "za wściekły");

        Rower[] tablica = {rower1, rower2};

        for (Rower rowery : tablica) {
            rowery.wypiszInformacjeORowerze();
        }

    }
}
