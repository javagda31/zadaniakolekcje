package com.sda.javagda31.zad_enum_3;

public enum RuchRobota {
    KROK_LEWA(10),
    KROK_PRAWA(10),
    RUCH_REKA_LEWA(5),
    RUCH_REKA_PRAWA(5),
    SKOK(20);

    private int zuzycieBaterii;

    RuchRobota(int zuzycieBaterii) {
        this.zuzycieBaterii = zuzycieBaterii;
    }

    public int getZuzycieBaterii() {
        return zuzycieBaterii;
    }
}
