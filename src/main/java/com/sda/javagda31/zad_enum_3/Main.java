package com.sda.javagda31.zad_enum_3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Robot robot = new Robot("Marian"); // odpowiednio stworzony

        String komenda;

        do {
            System.out.println("Podaj komendę:");
            komenda = scanner.next();

            if (komenda.equalsIgnoreCase("wlacz")) {
                robot.włącz();
            } else if (komenda.equalsIgnoreCase("wylacz")) {
                robot.wyłącz();
            } else if (komenda.equalsIgnoreCase("naladuj")) {
                robot.naładuj();
            } else if (komenda.equalsIgnoreCase("porusz")) {
                System.out.println("Podaj rodzaj ruchu:");
                String ruch = scanner.next();
                RuchRobota ruchRobota = RuchRobota.valueOf(ruch.toUpperCase());
                robot.poruszRobotem(ruchRobota);
            }
        } while (!komenda.equalsIgnoreCase("quit"));
        System.out.println("Kończę pracę.");
    }
}
