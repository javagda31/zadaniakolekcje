package com.sda.javagda31.zad_enum_3;

public class Robot {
    private int poziomBaterii;
    private String nazwaRobota;
    private boolean wlaczony;

    public Robot(String nazwaRobota) {
        this.poziomBaterii = 100;
        this.nazwaRobota = nazwaRobota;
        this.wlaczony = false;
    }

    public void poruszRobotem(RuchRobota ruch) {
        if (!wlaczony) {
            System.out.println("Robot jest wyłączony, aby wykonać ruch, włącz robota.");
            return;
        }

        if (poziomBaterii >= ruch.getZuzycieBaterii()) {
            poziomBaterii -= ruch.getZuzycieBaterii();
            System.out.println("Wykonano ruch " + ruch + ", poziom baterii: " + poziomBaterii);
        } else {
            System.out.println("Niewystarczający poziom baterii. Wyłącz robota i go naładuj.");
        }
    }

    public void włącz() {
        if (wlaczony) {
            System.out.println("Robot jest już włączony.");
        } else {
            // onOff
            // boolean maybe_it_should_maybe_it_shouldnt = true;
            wlaczony = true;
            System.out.println("Uruchamiam robota.");
        }
    }

    public void wyłącz() {
        if (!wlaczony) {
            System.out.println("Robot jest już wyłączony.");
        } else {
            wlaczony = false;
            System.out.println("Wyłączam robota.");
        }
    }

    public void naładuj() {
        if (!wlaczony) {
            System.out.println("Ładuję robota.");
            poziomBaterii = 100;
        } else {
            System.out.println("Należy wyłączyć robota by go naładować.");
        }
    }
}
