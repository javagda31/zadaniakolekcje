package com.sda.javagda31.zad3;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<String> lista = Arrays.asList("1", "2", "3", "4");
        List<String> lista = new ArrayList<>(Arrays.asList(
                "10030",
                "3004",
                "4000",
                "12355",
                "12222",
                "67888",
                "111200",
                "225355",
                "2222",
                "1111",
                "3546",
                "138751",
                "235912"
        ));
        System.out.println(lista.get(0));

        lista.add("3"); // nie jest możliwe dodawanie elementów do listy stworzonej przez:
        // Arrays.asList

        System.out.println("Index elementu 138751:" + lista.indexOf("138751"));


        if (lista.contains("67888")) {
            System.out.println("Zawiera 67888");
        } else {
            System.out.println("Nie zawiera 67888");
        }
        if (lista.contains("67889")) {
            System.out.println("Zawiera 67889");
        } else {
            System.out.println("Nie zawiera 67889");
        }

        lista.remove("67888");
        lista.remove("67889");



        if (lista.contains("67888")) {
            System.out.println("Zawiera 67888");
        } else {
            System.out.println("Nie zawiera 67888");
        }
        if (lista.contains("67889")) {
            System.out.println("Zawiera 67889");
        } else {
            System.out.println("Nie zawiera 67889");
        }

        System.out.println();
        for (String s : lista) {
            System.out.print(s + " ");
        }
        System.out.println();
    }
}
