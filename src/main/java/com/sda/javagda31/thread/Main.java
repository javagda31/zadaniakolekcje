package com.sda.javagda31.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        new Thread(new Runnable() { // trudne i czasochłonne zadania - na cały okres życia aplikacji
            @Override
            public void run() {
                // opcja 1
                // jakie zadanie ma byc wykonane przez ten wątek
            }
        }).start();

//        ExecutorService pulaWątków = Executors.newFixedThreadPool();
        ExecutorService pulaWątków = Executors.newSingleThreadExecutor();

        ExecutorService pulaCached = Executors.newCachedThreadPool();

    }
}
