package com.sda.javagda31.set.zad2;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj słowo:");
        String slowo = scanner.next();

        System.out.println("Zawiera duplikaty:" + zawieraDuplikaty(slowo));
    }

    public static boolean zawieraDuplikaty(String text) {
        // todo: zwróć true jeśli tak (zawiera duplikaty)

        // 1 . dzielimy na char'y - iterujemy po literkach, a następnie umieszczamy literki
        // w secie. (iterujemy string i wyciągamy literki które dodajemy do setu)
        Set<Character> characters = new HashSet<>();
        for (int i = 0; i < text.length(); i++) {
            // wstaw literkę do setu.
            characters.add(text.charAt(i));
        }


        // 2 . porównanie wielkości string vs. set
        return characters.size() != text.length();
    }
}
