package com.sda.javagda31.set.zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Integer[] ints = {10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55};

        Set<Integer> integers = new TreeSet<>(Arrays.asList(ints));
        System.out.println("Rozmiar setu: " + integers.size());
        System.out.println("Elementy:");
        for (Integer integer : integers) {
            System.out.println(integer);
        }
        System.out.println();

        integers.remove(10);
        integers.remove(12);

        // ponownie
        System.out.println("Rozmiar setu: " + integers.size());
        System.out.println("Elementy:");
        for (Integer integer : integers) {
            System.out.println(integer);
        }
        System.out.println();

    }
}
