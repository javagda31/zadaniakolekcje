package com.sda.javagda31.set.zad3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ParaLiczb[] paraLiczbs = {
                new ParaLiczb(1, 2), //
                new ParaLiczb(2, 1),
                new ParaLiczb(1, 1),
                new ParaLiczb(1, 2), //
        };

        Set<ParaLiczb> set = new HashSet<>(Arrays.asList(paraLiczbs));
        System.out.println(set);
    }
}
