package com.sda.javagda31.set.zad3;

import lombok.*;

import java.util.Objects;

//@Data // gettery settery, equals and hashcode, toString, required args constructor
@ToString
@Getter
@Setter
@AllArgsConstructor
public class ParaLiczb {
    private int a, b;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParaLiczb paraLiczb = (ParaLiczb) o;
        return a == paraLiczb.a &&
                b == paraLiczb.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }
}
