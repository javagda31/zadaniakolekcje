package com.sda.javagda31.zad2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        List<Integer> list = new ArrayList<>();
        // załaduj 10 randomowych liczb
        for (int i = 0; i < 10; i++) {
            list.add(random.nextInt(100));
        }

        // wypisz je
        System.out.println(list); // []

        double suma = 0;
        for (Integer wartoscWLiscie : list) {
            suma += wartoscWLiscie;
        }

        System.out.println("Suma = " + suma);
        System.out.println("Średnia = " + (suma / list.size()));

        List<Integer> kopia = new ArrayList<>(list); // stworzenie kopii listy list.
        Collections.sort(kopia);

        double mediana;
        if (kopia.size() % 2 == 0) {
            mediana = (kopia.get(kopia.size() / 2) + kopia.get(kopia.size() / 2 + 1)) / 2.0;
        } else {
            mediana = kopia.get(kopia.size() / 2);
        }
        System.out.println("Mediana = " + mediana);

        int max, min;
        max = min = list.get(0);

        for (int i = 1; i < list.size(); i++) {
            if (min > list.get(i)) {
                min = list.get(i);
            }
            if (max < list.get(i)) {
                max = list.get(i);
            }
        }

        System.out.println("Max = " + max);
        System.out.println("Min = " + min);

        int maxIndex, minIndex;
        maxIndex = minIndex = 0;

        for (int i = 0; i < list.size(); i++) {
            if (min == list.get(i)) {
                minIndex = i;
            }
            if (max == list.get(i)) {
                maxIndex = i;
            }
        }

        System.out.println("Max index = " + maxIndex);
        System.out.println("Min index = " + minIndex);

        // 3
        System.out.println("Max index = " + list.indexOf(max));
        System.out.println("Min index = " + list.indexOf(min));
    }

}
