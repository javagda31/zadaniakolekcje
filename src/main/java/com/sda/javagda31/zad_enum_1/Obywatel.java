package com.sda.javagda31.zad_enum_1;

import com.sda.javagda31.zad5.Plec;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Obywatel {
    private String imie;
    private String nazwisko;
    private String pesel;
    private Plec plec;

    @Override
    public String toString() {
        return "Obywatel{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", pesel='" + pesel + '\'' +
                ", plec=" + plec +
                '}';
    }
}
