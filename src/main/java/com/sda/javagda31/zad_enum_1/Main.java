package com.sda.javagda31.zad_enum_1;

import com.sda.javagda31.zad5.Plec;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        String imie = pobierzString("Podaj imie", -1);
        System.out.println("Podaj imie:");
        String imie = scanner.next();
//        String nazwisko = pobierzString("Podaj nazwisko", -1);
        System.out.println("Podaj nazwisko:");
        String nazwisko = scanner.next();
//        String pesel = pobierzString("Podaj pesel", -1);
        System.out.println("Podaj pesel:");
        String pesel = scanner.next();

        System.out.println("Podaj plec:");
        String plecString = scanner.next();

        Plec plec = Plec.valueOf(plecString.toUpperCase());

        Obywatel obywatel = new Obywatel(imie, nazwisko, pesel, plec);
        System.out.println(obywatel);
    }

    public static Long pobierzLong(String tresc, int dlugosc) {
        Scanner scanner = new Scanner(System.in);
        Long wartosc = null;
        do {
            System.out.println(tresc);
            try {
                wartosc = scanner.nextLong();
                if (String.valueOf(wartosc).length() < dlugosc) {
                    throw new InputMismatchException("Wrong length");
                }
            } catch (InputMismatchException ime) {
                if (ime.getMessage() == null) {
                    String bledneSlowo = scanner.next(); // skip wrong token
                    System.out.println("Twoja błędna treść brzmi: " + bledneSlowo);
                }
                wartosc = null;
                System.out.println("Try again, length(" + dlugosc + "):");
            }
        } while (wartosc == null);

        scanner.close();
        return wartosc;
    }

    public static String pobierzString(String tresc, int dlugosc) {
        Scanner scanner = new Scanner(System.in);
        String wartosc = null;
        do {
            System.out.println(tresc);
            try {
                wartosc = scanner.next();
                if (dlugosc != -1 && wartosc.length() < dlugosc) {
                    throw new InputMismatchException("Wrong length");
                }
            } catch (InputMismatchException ime) {
                if (ime.getMessage() == null) {
                    String bledneSlowo = scanner.next(); // skip wrong token
                    System.out.println("Twoja błędna treść brzmi: " + bledneSlowo);
                }
                wartosc = null;
                System.out.println("Try again, length(" + dlugosc + "):");
            }
        } while (wartosc == null);

        scanner.close();
        return wartosc;
    }
}
