package com.sda.javagda31.kolekcje;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 1000000; i++) {
            list.add(i);
        }

        long start = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) { // n
            int ile = list.get(i);              // n
            ile++;
        }

        long stop = System.currentTimeMillis();
        System.out.println("Czas wykonania: " + (stop - start));
    }
}
