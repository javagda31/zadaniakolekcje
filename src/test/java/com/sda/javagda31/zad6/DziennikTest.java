package com.sda.javagda31.zad6;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(JUnit4.class)
public class DziennikTest {
    private static List<Student> students = new ArrayList<>(Arrays.asList(
            new Student("123", "Marian", "Kowalski"),
            new Student("342", "Katarzyna", "Kowalska"),
            new Student("568", "Włodek", "Smith"),
            new Student("367", "Marcin", "Nowak"),
            new Student("273", "Olek", "Dębski"),
            new Student("299", "Juliusz", "Brzęczyszczykiewicz"),
            new Student("929", "Maryla", "Dębska")
    ));

    private Dziennik dziennik;

    @Before
    public void setUp() throws Exception {
        dziennik = new Dziennik();
        for (Student student : students) {
            dziennik.dodajStudenta(student);
        }
    }

    @Test
    public void test_szukanie(){
        Optional<Student> studentOptional = dziennik.zwrocStudenta("123");
        Assert.assertTrue(studentOptional.isPresent());

        Student student = studentOptional.get();

        Assert.assertEquals("Marian", student.getImie());
        Assert.assertEquals("Kowalski", student.getNazwisko());
        Assert.assertEquals("123", student.getIndeks());
    }
}
